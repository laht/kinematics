/*
 * Copyright 2014 Aalesund University College.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package info.laht.kinematics;

import info.laht.kinematics.joints.Joint;
import info.laht.kinematics.joints.JointNode;
import info.laht.kinematics.joints.JointType;
import info.laht.utilitypack.math.Matrix4x4;
import info.laht.utilitypack.math.TransformData;
import info.laht.utilitypack.math.Vector3d;
import info.laht.utilitypack.math.Limit;
import info.laht.utilitypack.utils.DoubleArray;
import info.laht.utilitypack.utils.TickTock;
import java.util.ArrayList;
import java.util.List;
import org.ejml.simple.SimpleMatrix;

/**
 * KinematicChain backed by scenegraph nodes
 * @author Lars Ivar Hatledal
 */
public class KinematicNodeChain extends KinematicChain {

    private final List<JointNode> nodes;
    private final List<JointNode> actuableNodes;

    public KinematicNodeChain(KinematicChain chain) {
        super(chain);

        this.nodes = new ArrayList<>(size());
        this.actuableNodes = new ArrayList<>(getNumDOF());

        int i = 0;
        JointNode prev = null;
        for (Joint t : this) {
            JointNode node = new JointNode(i++, t);
            nodes.add(node);
            if (!t.isFixed()) {
                actuableNodes.add(node);
            }
            if (prev != null) {
                prev.attachChild(node);
            }
            prev = node;
        }
    }

    /**
     * Get the joint value of joint i
     *
     * @param i 0 indexed joint position
     * @return the value of joint i
     */
    public double getJointValue(int i) {
        return actuableNodes.get(i).getValue();
    }

    /**
     * Get the joint values. Metric values for prismatic joints. Degrees for
     * revolute ones
     *
     * @return the joint values
     */
    public DoubleArray getJointValues() {
        double[] result = new double[getNumDOF()];
        int i = 0;
        for (JointNode node : actuableNodes) {
            result[i++] = node.getValue();
        }
        return new DoubleArray(result);
    }

    /**
     * Set the joint values. Metric values for prismatic joints. Degrees for
     * revolute ones
     *
     * @param values the values to set.
     */
    public void setJointValues(DoubleArray values) {
        if (values.size() != getNumDOF()) {
            throw new IllegalArgumentException("The length of the input must be equal to the number of DOF!");
        }
        int i = 0;
        for (JointNode node : actuableNodes) {
            node.setValue(values.get(i++));
        }
    }

    /**
     * Get the transform data for the last node in the chain
     * @return he transform data for the last node in the chain
     */
    public TransformData getEndEffectorState() {
        return nodes.get(nodes.size() - 1).getWorldTransformation();
    }

    /**
     * Get all node states as a list
     * @return all node states as a list
     */
    public List<TransformData> getStates() {
        List<TransformData> list = new ArrayList<>();
        nodes.stream().forEach((node) -> {
            list.add(node.getWorldTransformation());
        });
        return list;
    }

    /**
     * Get the numerical jacobian matrix
     *
     * @param full whether or not we want the (half) 3xn or (full) 6xn matrix
     * @return the numerical jacobian matrix
     */
    public SimpleMatrix getJacobian(boolean full) {
        return super.getJacobian(getJointValues(), full);
    }

    /**
     * Calculates the static joint torques
     *
     * @param force the force vector acting on the end-effector
     * @return the static joint torques
     */
    public DoubleArray getStaticJointTorques(Vector3d force) {
        return super.getStaticJointTorques(getJointValues(), force);
    }

    /**
     * Calculates the static force exerted on the end-effector
     *
     * @param torques the joint torques
     * @return the static force
     */
    public Vector3d getStaticForce(DoubleArray torques) {
        return super.getStaticForce(getJointValues(), torques);
    }

    /**
     * Evaluates the symbolic jacobian given the current jointvalues
     * @return the symbolic jacobian
     */
    public SimpleMatrix evaluateJacobian() {
        return super.evaluateJacobian(getJointValues());
    }

    /**
     * Evaluates the symbolic FK given the current jointvalues
     * @return the symbolic FK
     */
    public Matrix4x4 evaluateFK() {
        return super.evaluateFK(getJointValues());
    }

    /**
     * Get all jointNnodes
     * @return all jointNodes
     */
    public List<JointNode> getNodes() {
        return nodes;
    }

    /**
     * Get the jointNodes that are actuable. I.e not FIXED
     * @return the actuable joints
     */
    public List<JointNode> getActuableNodes() {
        return actuableNodes;
    }

    public static void main(String[] args) {
        KinematicChain chain = new KinematicChain("Crane3R");
        chain.addJoint(JointType.RY, Vector3d.ZEROS, new Limit(-180, 180));
        chain.addJoint(JointType.RX, Vector3d.Y.multiply(2.61), new Limit(-92, 0));
        chain.addJoint(JointType.RX, Vector3d.Z.multiply(7.01), new Limit(45, 150));
        chain.addFixedJoint(Vector3d.Z.multiply(3.61));

        DoubleArray vals = new DoubleArray(new double[]{10, -50, 100});

        System.out.println(chain.evaluateFK(vals));
        System.out.println(chain.getStates(null, vals));

        System.out.println(chain.getJacobian(vals, false));
        System.out.println(chain.evaluateJacobian(vals));

        System.out.println("----------------------------------");

        KinematicNodeChain nodes = new KinematicNodeChain(chain);
        nodes.setJointValues(vals);

        System.out.println(nodes.getJointValues());

        System.out.println(nodes.evaluateFK());
        System.out.println(nodes.getEndEffectorState());

        System.out.println(nodes.getJacobian(false));
        System.out.println(nodes.evaluateJacobian());
        TickTock tick = new TickTock();
        for (int i = 0; i < 10; i++) {

            nodes.evaluateFK();

        }
        System.out.println(tick.tock());

        tick = new TickTock();
        for (int i = 0; i < 10; i++) {

            nodes.getEndEffectorState();

        }
        System.out.println(tick.tock());

    }

}
