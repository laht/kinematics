/*
 * Copyright 2015 Aalesund University College
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package info.laht.kinematics.ik;

import info.laht.kinematics.KinematicChain;
import info.laht.utilitypack.math.TransformData;
import info.laht.utilitypack.math.Vector3d;
import info.laht.utilitypack.utils.DoubleArray;
import org.ejml.simple.SimpleMatrix;

/**
 * Solves the IK using the Damped Least Sqaured method
 * http://www.math.ucsd.edu/~sbuss/ResearchWeb/ikmethods/iksurvey.pdf
 *
 * @author Lars Ivar Hatledal
 */
public class DLSSolver implements IKSolver {

    private KinematicChain chain;
    private double lambda;

    public DLSSolver(KinematicChain chain) {
        this(chain, 0.01);
    }

    public DLSSolver(KinematicChain chain, double lambda) {
        this.chain = chain;
        this.lambda = lambda;
    }

    @Override
    public DoubleArrayCandidate solveIK(DoubleArray start, Vector3d targetPos, long solveTime) {
        long t0 = System.currentTimeMillis();
        DoubleArray result = start == null ? chain.getMeanJointValues() : start;

        do {
            SimpleMatrix J = chain.getJacobian(result, false);
            SimpleMatrix I = SimpleMatrix.identity(3);
            SimpleMatrix Jt = J.transpose();
            SimpleMatrix JJt = J.mult(Jt);
            SimpleMatrix inv = Jt.mult((JJt.plus(Math.pow(lambda, 2), I)).invert());

            Vector3d actual = chain.getEndEffectorState(null, result).getTranslation();
            Vector3d dX = targetPos.sub(actual);

            SimpleMatrix mult = inv.mult(new SimpleMatrix(new double[][]{{dX.getX()}, {dX.getY()}, {dX.getZ()}}));

            for (int i = 0; i < mult.numRows(); i++) {
                result.set(i, chain.getLimits().get(i).clamp(result.get(i) + mult.get(i, 0)));
            }

        } while (System.currentTimeMillis() - t0 < solveTime);

        return new DoubleArrayCandidate(result.asArray(), 0);
    }

    @Override
    public DoubleArrayCandidate solveIK(DoubleArray start, Vector3d targetPos, Vector3d targetRot, long solveTime) {
        long t0 = System.currentTimeMillis();
        DoubleArray result = start == null ? chain.getMeanJointValues() : start;

        do {
            SimpleMatrix J = chain.getJacobian(result, true);
            SimpleMatrix I = SimpleMatrix.identity(6);
            SimpleMatrix Jt = J.transpose();
            SimpleMatrix JJt = J.mult(Jt);
            SimpleMatrix inv = Jt.mult((JJt.plus(Math.pow(lambda, 2), I)).invert());

            TransformData tm = chain.getEndEffectorState(null, result);
            Vector3d actualPos = tm.getTranslation();
            Vector3d actualRot = tm.getEulerAngles();
            Vector3d dX1 = targetPos.sub(actualPos);
            Vector3d dX2 = targetRot.sub(actualRot);

            SimpleMatrix mult = inv.mult(new SimpleMatrix(new double[][]{{dX1.getX()}, {dX1.getY()}, {dX1.getZ()}, {dX2.getX()}, {dX2.getY()}, {dX2.getZ()}}));

            for (int i = 0; i < mult.numRows(); i++) {
                result.set(i, chain.getLimits().get(i).clamp(result.get(i) + mult.get(i, 0)));
            }

        } while (System.currentTimeMillis() - t0 < solveTime);

        return new DoubleArrayCandidate(result.asArray(), 0);
    }

    @Override
    public String toString() {
        return "DLSSolver{" + "lambda=" + lambda + '}';
    }
    
    

}
