/*
 * Copyright 2015 Aalesund University College
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package info.laht.kinematics.ik;

import info.laht.utilitypack.math.Vector3d;
import info.laht.utilitypack.utils.DoubleArray;

/**
 * 
 * @author Lars Ivar Hatledal
 */
public interface IKSolver {


    /**
     * IK solver
     *
     * @param start (optional) starting joint configuration. Metric value for
     * prismatic joints. Degrees for revolute ones
     * @param targetPos target position
     * @param solveTime maximum solve time
     * @return the joint values needed to reach targetPos
     */
    public DoubleArrayCandidate solveIK(DoubleArray start, Vector3d targetPos, long solveTime);
    
     /**
     * IK solver
     *
     * @param start (optional) starting joint configuration. Metric value for
     * prismatic joints. Degrees for revolute ones
     * @param targetPos target position
     * @param targetRot target rotation (in degrees)
     * @param solveTime maximum solve time
     * @return the joint values needed to reach targetPos
     */
    public DoubleArrayCandidate solveIK(DoubleArray start, Vector3d targetPos, Vector3d targetRot, long solveTime);

}
