/**
 * Copyright 2014 Aalesund University College
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */
package info.laht.kinematics.ik;

import info.laht.utilitypack.utils.DoubleArray;
import java.util.Arrays;

/**
 *
 * @author Lars Ivar Hatledal
 */
public class DoubleArrayCandidate extends DoubleArray implements Comparable<DoubleArrayCandidate> {

    private double cost;

    public DoubleArrayCandidate(int length) {
        this(new double[length]);
    }

    public DoubleArrayCandidate(double[] array) {
        this(array, Double.MAX_VALUE);
    }

    public DoubleArrayCandidate(double[] array, double cost) {
        super(array);
        this.cost = cost;
    }

    /**
     * Updates the cost of the candidate
     * @param cost the new cost
     */
    public void setCost(double cost) {
        this.cost = cost;
    }

    /**
     * Get the cost of this candidate
     * @return the cost. I.e performance (lower is better)
     */
    public double getCost() {
        return cost;
    }

    @Override
    public DoubleArrayCandidate copy() {
        return new DoubleArrayCandidate(asArray().clone(), cost);
    }

    @Override
    public String toString() {
        return "DoubleArrayCandidate{" + "cost=" + cost + ", array=" + Arrays.toString(asArray()) + '}';
    }

    @Override
    public int compareTo(DoubleArrayCandidate o) {
        if (cost == o.getCost()) {
            return 0;
        } else if (cost > o.getCost()) {
            return -1;
        } else {
            return 1;
        }
    }

}
