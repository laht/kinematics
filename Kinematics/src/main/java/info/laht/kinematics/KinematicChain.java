/*
 * Copyright 2014 Lars Ivar Hatledal <larsivarhatledal@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package info.laht.kinematics;


import info.laht.kinematics.ik.DLSSolver;
import info.laht.kinematics.ik.DoubleArrayCandidate;
import info.laht.kinematics.ik.IKSolver;
import info.laht.kinematics.joints.Joint;
import info.laht.kinematics.joints.JointType;
import info.laht.kinematics.workspace.Workspace;
import info.laht.utilitypack.math.DualQuaternion;
import info.laht.utilitypack.math.Matrix4x4;
import info.laht.utilitypack.math.TransformData;
import info.laht.utilitypack.math.Vector3d;
import info.laht.utilitypack.math.Limit;
import info.laht.utilitypack.math.MathUtil;
import info.laht.utilitypack.utils.AbstractIterable;
import info.laht.utilitypack.utils.ArrayUtil;
import info.laht.utilitypack.utils.DoubleArray;
import info.laht.utilitypack.utils.IDGenerator;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.ejml.simple.SimpleMatrix;

/**
 * Class representing a kinematic chain.
 *
 * @author Lars Ivar Hatledal
 */
public class KinematicChain extends AbstractIterable<Joint> {

    private static final Logger LOG = Logger.getLogger(KinematicChain.class.getName());

    public final int ID = IDGenerator.getInstance().nextID();

    private final List<JointType> jointTypes;
    private final List<Vector3d> relativePositions;
    private final List<Limit> limits;

    private final List<Joint> joints;
    private final List<Joint> actuableJoints;

    private String name;
    private IKSolver ikSolver;
    private Workspace workspace;
    private SymbolicKinematics syms;
    private boolean dirtyWorkspace = true;

    public KinematicChain(String name) {
        this(name, new ArrayList<>(), new ArrayList<>(), new ArrayList<>());
    }

    public KinematicChain(String name, List<JointType> jointTypes, List<Vector3d> relativePositions, List<Limit> limits) {
        this.name = name;
        this.joints = new ArrayList<>();
        this.actuableJoints = new ArrayList<>();
        this.jointTypes = new ArrayList<>();
        this.relativePositions = new ArrayList<>();
        this.limits = new ArrayList<>();

        int j = 0;
        int len = jointTypes.size();
        for (int i = 0; i < len; i++) {
            JointType jt = jointTypes.get(i);
            Vector3d relPos = relativePositions.get(i);
            if (JointType.isActuable(jt)) {
                addJoint(jt, relPos, limits.get(j++));
            } else {
                addJoint(jt, relPos, null);
            }
        }
        this.ikSolver = new DLSSolver(this);
    }

    protected KinematicChain(KinematicChain chain) {
        this.name = chain.name;
        this.joints = chain.joints;
        this.actuableJoints = chain.actuableJoints;
        this.jointTypes = chain.jointTypes;
        this.relativePositions = chain.relativePositions;
        this.limits = chain.limits;
        this.dirtyWorkspace = chain.dirtyWorkspace;
        this.workspace = chain.workspace;
        this.syms = chain.syms;
        this.ikSolver = new DLSSolver(this);
    }

    /**
     * Get name
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Set the name
     *
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Adds a fixed joint
     *
     * @param relativePosition the relative position to the previous joint
     */
    public void addFixedJoint(Vector3d relativePosition) {
        addJoint(JointType.FIXED, relativePosition, null);
    }

    /**
     * Adds a joint
     *
     * @param jointType the jointType
     * @param relativePosition the relative position to the previous joint
     * @param lim the joint limit or null for fixed joints
     */
    public void addJoint(JointType jointType, Vector3d relativePosition, Limit lim) {

        jointTypes.add(jointType);
        relativePositions.add(relativePosition);
        if (lim != null) {
            limits.add(lim);
        }

        Joint joint = new Joint(jointType, relativePosition, lim);
        if (joint.isActuable()) {
            actuableJoints.add(joint);
        }
        joints.add(joint);

        dirtyWorkspace = true;
        syms = null;
    }

    /**
     * Removes a joint
     *
     * @return the removed joint or null if there is nothing to remove
     */
    public Joint removeJoint() {
        if (size() > 0) {
            Joint remove = joints.remove(joints.size() - 1);
            if (remove.isActuable()) {
                actuableJoints.remove(actuableJoints.size() - 1);
            }
            dirtyWorkspace = true;
            syms = null;
            return remove;
        }
        LOG.log(Level.WARNING, "Populate the chain before tryng to remove anything!");
        return null;
    }

    /**
     * Evaluates the symbolic FK. This method is slower than the numerical one..
     *
     * @param values the values to use
     * @return the result or null if it isn't available
     */
    public final Matrix4x4 evaluateFK(DoubleArray values) {
        if (syms == null) {
            LOG.log(Level.INFO, "Syms has not been created. Doing that now..");
            try {
                syms = new SymbolicKinematics(this);
            } catch (IOException ex) {
                LOG.log(Level.SEVERE, "Syms FAILED. Is Python w/SymPy innstalled and added to PATH?");
                Logger.getLogger(KinematicChain.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return syms == null ? null : syms.evaluateFK(values);

    }

    /**
     * Evaluates the symbolic Jacobian. This method is slower than the numerical
     * one..
     *
     * @param values the values to use
     * @return the result or null if it isn't available
     */
    public final SimpleMatrix evaluateJacobian(DoubleArray values) {
        if (syms == null) {
            LOG.log(Level.INFO, "Syms has not been created. Doing that now..");
            try {
                syms = new SymbolicKinematics(this);
            } catch (IOException ex) {
                LOG.log(Level.SEVERE, "Syms FAILED. Is Python w/SymPy innstalled and added to PATH?");
                Logger.getLogger(KinematicChain.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return syms == null ? null : syms.evaluateJacobian(values);
    }

    /**
     * Get the symbolic FK equation
     *
     * @return or null if it isn't available
     */
    public final String getSymbolicFK() {
        if (syms == null) {
            LOG.log(Level.INFO, "Syms has not been created. Doing that now..");
            try {
                syms = new SymbolicKinematics(this);
            } catch (IOException ex) {
                LOG.log(Level.SEVERE, "Syms FAILED. Is Python w/SymPy innstalled and added to PATH?");
                Logger.getLogger(KinematicChain.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return syms == null ? "Undefined" : syms.getFK();
    }

    /**
     * Get the symbolic Jacobian equation
     *
     * @return or null if it isn't available
     */
    public final String getSymbolicJacobian() {
        if (syms == null) {
            LOG.log(Level.INFO, "Syms has not been created. Doing that now..");
            try {
                syms = new SymbolicKinematics(this);
            } catch (IOException ex) {
                LOG.log(Level.SEVERE, "Syms FAILED. Is Python w/SymPy innstalled and added to PATH?");
                Logger.getLogger(KinematicChain.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return syms == null ? "Undefined" : syms.getJacobian();
    }

    /**
     * Get both the FK and Jacobian
     *
     * @return the FK and Jacobian as one string
     */
    public String getSymbolicEquations() {
        StringBuilder sb = new StringBuilder();
        sb.append("FK:").append("\n")
                .append(getSymbolicFK()).append("\n")
                .append("Jacobian:").append("\n")
                .append(getSymbolicJacobian());

        return sb.toString();
    }

    /**
     * Calculates the manipulator Jacobian
     *
     * @param values the joint values in degrees
     * @param full true if J is a 6xn matrix, false if J is a 3xn matrix
     * @return the numerical Jacobian
     */
    public final SimpleMatrix getJacobian(DoubleArray values, boolean full) {
        values = values.clamp(getLimits());

        int rows = full ? 6 : 3;
        int cols = values.size();

        double h = 0.00000001;
        double[][] J = new double[rows][cols];

        TransformData trans = getEndEffectorState(null, values);

        Vector3d d1 = trans.getTranslation();
        Vector3d d3 = null;
        if (full) {
            d3 = trans.getEulerAngles();
        }

        for (int i = 0; i < cols; i++) {
            double[] vals = ArrayUtil.toRadians(values.asArray());
            vals[i] += h;

            trans = getEndEffectorState(null, new DoubleArray(ArrayUtil.toDegrees(vals)));

            Vector3d d2 = trans.getTranslation();

            J[0][i] = ((d2.getX() - d1.getX()) / h);
            J[1][i] = ((d2.getY() - d1.getY()) / h);
            J[2][i] = ((d2.getZ() - d1.getZ()) / h);

            if (full) {
                Vector3d d4 = trans.getEulerAngles();

                J[3][i] = ((d4.getX() - d3.getX()) / h);
                J[4][i] = ((d4.getY() - d3.getY()) / h);
                J[5][i] = ((d4.getZ() - d3.getZ()) / h);
            }
        }
        return new SimpleMatrix(J);
    }

    /**
     * Calculates the static force exerted on the end-effector
     *
     * @param values the joint values
     * @param torques the joint torques
     * @return the static force
     */
    public final Vector3d getStaticForce(DoubleArray values, DoubleArray torques) {
        SimpleMatrix J = getJacobian(values, false);
        SimpleMatrix Jt_inv = (J.numCols() == J.numRows()) ? ((J.determinant() > 0.0001) ? J.transpose().invert() : J.transpose().pseudoInverse()) : J.transpose().pseudoInverse();
        SimpleMatrix F = Jt_inv.mult(new SimpleMatrix(torques.asRowMatrix()));
        return new Vector3d(F.get(0, 0), F.get(1, 0), F.get(2, 0));
    }

    /**
     * Calculates the static joint torques
     *
     * @param values the joint values
     * @param f the force vector acting on the end-effector
     * @return the static joint torques
     */
    public final DoubleArray getStaticJointTorques(DoubleArray values, Vector3d f) {

        double[] torques = new double[getNumDOF()];
        double[] mass = new double[]{1500, 2200, 1300};

        List<TransformData> states = getStates(null, values);
        double h = 0.00000001;
        for (int i = 0; i < getNumDOF(); i++) {
            int rows = 3;
            int cols = i + 1;

            double[][] J = new double[rows][cols];

            Vector3d d1 = states.get(i).getTranslation().add(states.get(i + 1).getTranslation().sub(states.get(i).getTranslation()).divide(2));

            for (int j = 0; j < cols; j++) {
                double[] vals = ArrayUtil.toRadians(values.asArray().clone());
                vals[j] += h;

                List<TransformData> states1 = getStates(null, new DoubleArray(ArrayUtil.toDegrees(vals)));

                Vector3d d2 = states1.get(i).getTranslation().add(states1.get(i + 1).getTranslation().sub(states1.get(i).getTranslation()).divide(2));

                J[0][j] = ((d2.getX() - d1.getX()) / h);
                J[1][j] = ((d2.getY() - d1.getY()) / h);
                J[2][j] = ((d2.getZ() - d1.getZ()) / h);

            }

            SimpleMatrix Jt = new SimpleMatrix(J).transpose();
            SimpleMatrix mult = Jt.mult(new SimpleMatrix(new Vector3d(0, mass[i] * -9.81, 0).toRowMatrix3()));

            for (int k = 0; k < mult.numRows(); k++) {
                torques[k] += mult.get(k, 0);
            }
        }

        SimpleMatrix Jt = getJacobian(values, false).transpose();
        SimpleMatrix mult = Jt.mult(new SimpleMatrix(f.toRowMatrix3()));

        for (int i = 0; i < getNumDOF(); i++) {
            torques[i] += mult.get(i, 0);
        }
        
        if (torques[2] == 0) {
//            System.out.println(values);
        }
        
        return new DoubleArray(torques);
    }

    /**
     * Forward kinematics calculation using dual quaternions
     *
     * @param origin the global reference frame, or null to use the local
     * reference
     * @param values the joint values
     * @return the numerical forward kinematics
     */
    public final TransformData getEndEffectorState(TransformData origin, DoubleArray values) {
        values.clamp(getLimits());
        int i = 0;
        TransformData result = origin == null ? new DualQuaternion() : origin;
        for (Joint j : this) {
            TransformData q = j.getTransformation(j.isActuable() ? values.get(i++) : null);
            result = result.multiply(q);
        }
        return result;
    }

    /**
     * Get the transformation matrix for each joint in the chain
     *
     * @param origin the global reference frame, or null to use the local
     * reference
     * @param values the joint values
     * @return the transformation matrix for each joint in the chain
     */
    public final List<TransformData> getStates(TransformData origin, DoubleArray values) {
        values.clamp(getLimits());
        List<TransformData> list = new ArrayList<>(size());
        int i = 0;
        TransformData result = origin == null ? new DualQuaternion() : origin;
        for (Joint j : this) {
            TransformData tm = j.getTransformation(j.isActuable() ? values.get(i++) : null);
            result = result.multiply(tm);
            list.add(result);
        }
        return list;
    }

    /**
     * Generates a list of random allowable joint configurations
     *
     * @param num the number of configurations
     * @return a list of random allowable joint configurations
     */
    public final List<DoubleArray> getRandomJointConfigurations(int num) {
        List<DoubleArray> list = new ArrayList<>(num);
        for (int i = 0; i < num; i++) {
            list.add(new DoubleArray(getNumDOF()).randomize(getLimits()));
        }
        return list;
    }

    /**
     * Uses FK to convert the joint configurations into cartesian coordinates
     *
     * @param configurations the joint configurations to apply FK on
     * @return a list of cartesian coordinates
     */
    public final List<Vector3d> getEndEffectorPositions(List<DoubleArray> configurations) {
        List<Vector3d> list = new ArrayList<>(configurations.size());
        configurations.stream().forEach((conf) -> {
            list.add(getEndEffectorState(null, conf).getTranslation());
        });
        return list;
    }

    /**
     * Normalizes the input to be in the range [0-1]
     *
     * @param arr the array to normalize
     * @return a new normalized array
     */
    public double[] normalize(double[] arr) {
        double[] result = new double[getNumDOF()];
        for (int i = 0; i < result.length; i++) {
            Limit lim = limits.get(i);
            result[i] = MathUtil.map(lim.clamp(arr[i]), lim.getMin(), lim.getMax(), 0, 1);
        }
        return (result);
    }

    /**
     * Normalizes the input to be in the range [0-1]
     *
     * @param arr the array to normalize
     * @return a new normalized array
     */
    public DoubleArray normalize(DoubleArray arr) {
        return new DoubleArray(normalize(arr.asArray()));
    }

    /**
     * Denormalizes the input to be in the range [min-max] given by the joint
     * limits
     *
     * @param arr the array to denormalize
     * @return a new denormalized array
     */
    public double[] denormalize(double[] arr) {
        double[] result = new double[getNumDOF()];
        for (int i = 0; i < result.length; i++) {
            Limit lim = limits.get(i);
            result[i] = MathUtil.map(MathUtil.clamp(arr[i], 0, 1), 0, 1, lim.getMin(), lim.getMax());
        }
        return (result);
    }

    /**
     * Denormalizes the input to be in the range [min-max] given by the joint
     * limits
     *
     * @param arr the array to denormalize
     * @return a new denormalized array
     */
    public DoubleArray denormalize(DoubleArray arr) {
        return new DoubleArray(denormalize(arr.asArray()));
    }

    /**
     * Normalizes the input to be in the range [0-1]
     *
     * @param v the vector to normalize
     * @return a new normalized vector
     */
    public Vector3d normalize(Vector3d v) {
        Vector3d min = getWorkspace().getBoundingBox().getMin();
        Vector3d max = getWorkspace().getBoundingBox().getMax();

        double x = MathUtil.map(MathUtil.clamp(v.getX(), min.getX(), max.getX()), min.getX(), max.getX(), 0, 1);
        double y = MathUtil.map(MathUtil.clamp(v.getY(), min.getY(), max.getY()), min.getY(), max.getY(), 0, 1);
        double z = MathUtil.map(MathUtil.clamp(v.getZ(), min.getZ(), max.getZ()), min.getZ(), max.getZ(), 0, 1);

        return new Vector3d(x, y, z);
    }

    /**
     * Denormalizes the input to be in the range [min-max] given by the bounding
     * box
     *
     * @param v the vector to denormalize
     * @return a new denormalized vector
     */
    public Vector3d denormalize(Vector3d v) {
        Vector3d min = getWorkspace().getBoundingBox().getMin();
        Vector3d max = getWorkspace().getBoundingBox().getMax();

        double x = MathUtil.map(MathUtil.clamp(v.getX(), 0, 1), 0, 1, min.getX(), max.getX());
        double y = MathUtil.map(MathUtil.clamp(v.getY(), 0, 1), 0, 1, min.getY(), max.getY());
        double z = MathUtil.map(MathUtil.clamp(v.getZ(), 0, 1), 0, 1, min.getZ(), max.getZ());

        return new Vector3d(x, y, z);
    }

    /**
     * Get the mean joint values specified by the joint limits
     *
     * @return the mean joint values
     */
    public DoubleArray getMeanJointValues() {
        double[] result = new double[getNumDOF()];
        for (int i = 0; i < result.length; i++) {
            result[i] = limits.get(i).getMean();
        }
        return new DoubleArray(result);
    }

    /**
     * Get the minimum joint values specified by the joint limits
     *
     * @return the lower bound (minimum) joint values
     */
    public DoubleArray getMinJointValues() {
        double[] result = new double[getNumDOF()];
        for (int i = 0; i < result.length; i++) {
            result[i] = limits.get(i).getMin();
        }
        return new DoubleArray(result);
    }

    /**
     * Get the maximum joint values specified by the joint limits
     *
     * @return the upper bound (minimum) joint values
     */
    public DoubleArray getMaxJointValues() {
        double[] result = new double[getNumDOF()];
        for (int i = 0; i < result.length; i++) {
            result[i] = limits.get(i).getMax();
        }
        return new DoubleArray(result);
    }

    /**
     * Solves the IK
     *
     * @param start (optional) starting joint configuration. Metric value for
     * prismatic joints. Degrees for revolute ones
     * @param targetPos the position to solve for
     * @param solveTime for how long should we compute?
     * @return the joint values needed to reach posTarget
     */
    public DoubleArrayCandidate solveIK(DoubleArray start, Vector3d targetPos, long solveTime) {
        return ikSolver.solveIK(start, targetPos, solveTime);
    }

    /**
     * Solves the IK
     *
     * @param start (optional) starting joint configuration. Metric value for
     * prismatic joints. Degrees for revolute ones
     * @param posTarget the position to solve for
     * @param rotTarget the rotation to solve for in radians
     * @param solveTime for how long should we compute?
     * @return the joint values needed to reach v
     */
    public DoubleArrayCandidate solveIK(DoubleArray start, Vector3d posTarget, Vector3d rotTarget, long solveTime) {
        return ikSolver.solveIK(start, posTarget, rotTarget, solveTime);
    }

    /**
     * Get the IK solver currently in use
     *
     * @return the IK solver currently in use
     */
    public IKSolver getIKSolver() {
        return ikSolver;
    }

    /**
     * Sets this chain to use the supplied IK solver
     *
     * @param ikSolver the new IK solver to use
     */
    public void setIKSolver(IKSolver ikSolver) {
        this.ikSolver = ikSolver;
    }

    /**
     * Get the jointTypes
     *
     * @return the jointTypes
     */
    public final List<JointType> getJointTypes() {
        return jointTypes;
    }

    /**
     * Get the relativePositions
     *
     * @return the relativePositions
     */
    public final List<Vector3d> getRelativePositions() {
        return relativePositions;
    }

    /**
     * Get the limits
     *
     * @return the limits
     */
    public final List<Limit> getLimits() {
        return limits;
    }

    public void setLimit(int i, Limit limit) {
        limits.set(i, limit);
        get(i).setLimit(limit);
    }

    /**
     * Get the number of degrees of freedom (DOF) for this chain
     *
     * @return the DOF
     */
    public final int getNumDOF() {
        return actuableJoints.size();
    }

    /**
     * Get the joints
     *
     * @return the joints
     */
    public final List<Joint> getJoints() {
        return joints;
    }

    /**
     * Get the joints that that are actuable
     *
     * @return the actuable joints
     */
    public final List<Joint> getActuableJoints() {
        return actuableJoints;
    }

    /**
     * Computes the workspace using the provided parameters
     *
     * @param c the number of monte carlo points
     * @param n the number of grids on each dimension
     */
    public synchronized void computeWorkspace(int c, int n) {
        if (workspace == null) {
            LOG.log(Level.INFO, "No workspace available. Creating one..");
            workspace = new Workspace(this);
        }
        workspace.compute(c, n);
        dirtyWorkspace = false;
    }

    public synchronized boolean shouldAnalyseWorkspace() {
        if (workspace == null) {
            LOG.log(Level.INFO, "No workspace available. Creating one..");
            workspace = new Workspace(this);
        }
        return workspace.shouldAnalyseWorkspace();
    }

    public synchronized void setShouldAnalyseWorkspace(boolean shouldAnalyseWorkspace) {
        if (workspace == null) {
            LOG.log(Level.INFO, "No workspace available. Creating one..");
            workspace = new Workspace(this);
        }
        workspace.setShouldAnalyseWorkspace(shouldAnalyseWorkspace);
    }

    /**
     * Get the workspace of this. The workspace is calculated the first time
     * this is invoked
     *
     * @return the workspace
     */
    public final synchronized Workspace getWorkspace() {
        if (workspace == null) {
            LOG.log(Level.INFO, "No workspace available. Creating one..");
            workspace = new Workspace(this);
        }

        if (dirtyWorkspace) {
            LOG.log(Level.INFO, "Workspace is dirty.. Computing new one");
            workspace.compute();
            dirtyWorkspace = false;
        }

        return workspace;
    }

    @Override
    public final Joint get(int i) {
        return joints.get(i);
    }

    @Override
    public final int size() {
        return joints.size();
    }
}
