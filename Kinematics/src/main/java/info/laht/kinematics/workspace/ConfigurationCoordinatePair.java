/*
 * Copyright 2014 Aalesund University College
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package info.laht.kinematics.workspace;

import info.laht.utilitypack.math.Vector3d;
import info.laht.utilitypack.utils.DoubleArray;

/**
 * Sortable configuration coordinate pair
 *
 * @author Lars Ivar Hatledal
 */
public class ConfigurationCoordinatePair implements Comparable<ConfigurationCoordinatePair> {

    private Vector3d sortByAxis;

    private final DoubleArray conf;
    private final Vector3d coord;

    public ConfigurationCoordinatePair(DoubleArray conf, Vector3d coord) {
        this.conf = conf;
        this.coord = coord;
        this.sortByAxis = Vector3d.X;
    }

    /**
     * Get the configuration values
     *
     * @return the joint configuration
     */
    public DoubleArray getConf() {
        return conf;
    }

    /**
     * Get the position that corresponds to the configuration
     *
     * @return the position that corresponds to the configuration
     */
    public Vector3d getCoord() {
        return coord;
    }

    /**
     * Which axis we are sorting by when sorting
     *
     * @return the axis we are sorting by when sorting
     */
    public Vector3d getSortByAxis() {
        return sortByAxis;
    }

    /**
     * Set which axis we are sorting by when sorting
     *
     * @param sortByAxis the axis to sort by
     */
    public void setSortByAxis(Vector3d sortByAxis) {
        this.sortByAxis = sortByAxis;
    }

    @Override
    public int compareTo(ConfigurationCoordinatePair o) {

        if (sortByAxis == Vector3d.X) {
            if (coord.getX() == o.getCoord().getX()) {
                return 0;
            } else if (coord.getX() > o.getCoord().getX()) {
                return -1;
            } else {
                return 1;
            }
        } else if (sortByAxis == Vector3d.Y) {
            if (coord.getY() == o.getCoord().getY()) {
                return 0;
            } else if (coord.getY() > o.getCoord().getY()) {
                return -1;
            } else {
                return 1;
            }
        } else if (sortByAxis == Vector3d.Z) {
            if (coord.getZ() == o.getCoord().getZ()) {
                return 0;
            } else if (coord.getZ() > o.getCoord().getZ()) {
                return -1;
            } else {
                return 1;
            }
        } else {
            throw new IllegalArgumentException("Unexcpected sortByAxis! Was: " + sortByAxis);
        }
    }
}
