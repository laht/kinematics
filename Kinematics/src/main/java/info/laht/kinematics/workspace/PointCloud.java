/*
 * Copyright 2015 Aalesund University College
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package info.laht.kinematics.workspace;

import info.laht.utilitypack.math.Vector3d;
import java.util.List;

/**
 *
 * @author Lars Ivar Hatledal
 */
public class PointCloud {

    private final String name;
    private final String plyHeader;
    private final List<Voxel> voxels;

    public PointCloud(String name, List<Voxel> voxels) {
        this.name = name;
        this.voxels = voxels;
        this.plyHeader = "ply \n"
                + "format ascii 1.0 \n"
                + "element vertex " + voxels.size() + "\n"
                + "property float x \n"
                + "property float y \n"
                + "property float z \n"
                + "property float nx \n"
                + "property float ny \n"
                + "property float nz \n"
                + "property uchar red \n"
                + "property uchar green \n"
                + "property uchar blue \n"
                + "end_header \n";
    }

    public String getName() {
        return name;
    }
    
    public String asPLYFormattedString() {
        StringBuilder sb = new StringBuilder();
        for (Voxel vox : voxels) {
            Vector3d v = vox.getCenter();
            Vector3d n = vox.getNormal();
            sb.append(v.getX()).append(" ").append(v.getY()).append(" ").append(v.getZ()).append(" ")
                    .append(n.getX()).append(" ").append(n.getY()).append(" ").append(n.getZ()).append(" ")
                    .append(vox.getColor().getRed()).append(" ").append(vox.getColor().getGreen()).append(" ").append(vox.getColor().getBlue()).append("\n");
        }

        return plyHeader + sb.toString();
    }

}
