/*
 * Copyright 2014 Lars Ivar Hatledal <larsivarhatledal@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package info.laht.kinematics.workspace;

import info.laht.utilitypack.math.Box3;
import info.laht.utilitypack.math.Vector3d;
import java.awt.Color;

/**
 * A Voxel represent a value on a regular grid
 *
 * @author Lars Ivar Hatledal
 */
public class Voxel extends Box3 {

    private Color color;
    private Vector3d normal;
    private boolean reachable, onBoundary, containsSingularity, withinSWL, withinWWL;

    public Voxel(Vector3d min, Vector3d max) {
        super(min, max);
        this.normal = new Vector3d(0, 0, 0);
        this.color = new Color(0, 255, 0);
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public boolean isReachable() {
        return reachable;
    }

    public void setReachable(boolean reachable) {
        this.reachable = reachable;
    }

    public boolean isOnBoundary() {
        return onBoundary;
    }

    public void setOnBoundary(boolean onBoundary) {
        this.onBoundary = onBoundary;
    }

    public boolean containsSingularity() {
        return containsSingularity;
    }

    public void setContainsSingularity(boolean containsSingularity) {
        this.containsSingularity = containsSingularity;
    }

    public boolean isWithinSWL() {
        return withinSWL;
    }

    public boolean isWithinWWL() {
        return withinWWL;
    }

    public void setNormal(Vector3d normal) {
        this.normal = normal;
    }

    public void setIsWithinSWL(boolean withinSWL) {
        this.withinSWL = withinSWL;
    }

    public void setIsWithinWWL(boolean withinWWL) {
        this.withinWWL = withinWWL;
    }

    Vector3d getNormal() {
        return normal;
    }

    @Override
    public String toString() {
        return "Voxel{" + "color=" + color + ", normal=" + normal + ", reachable=" + reachable + ", onBoundary=" + onBoundary + ", containsSingularity=" + containsSingularity + ", withinSWL=" + withinSWL + ", withinWWL=" + withinWWL + '}';
    }

}
