/*
 * Copyright 2014 Lars Ivar Hatledal <larsivarhatledal@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package info.laht.kinematics.workspace;

import info.laht.utilitypack.math.MathUtil;
import info.laht.utilitypack.utils.DoubleArray;
import info.laht.utilitypack.utils.TickTock;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;
import info.laht.kinematics.KinematicChain;
import info.laht.utilitypack.math.Box3;
import info.laht.utilitypack.math.Vector3d;
import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.function.Predicate;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Represent the workspace of a KinematicChain
 *
 * @author Lars Ivar Hatledal
 */
public class Workspace implements Serializable {

    private static final Logger LOG = Logger.getLogger(Workspace.class.getName());

    private final static int DEFAULT_C = 100000;
    private final static int DEFAULT_N = 25;

    private final transient ExecutorService executor = Executors.newCachedThreadPool();
    private final transient ExecutorCompletionService completionService = new ExecutorCompletionService<>(executor);

    private final KinematicChain chain;

    private Voxel[][][] voxels;
    private final Box3 boundingBox;

    private boolean shouldAnalyseWorkspace;

    private String mesh;
    private double volume;

    public Workspace(KinematicChain chain) {
        this.chain = chain;
        this.boundingBox = new Box3();
        this.shouldAnalyseWorkspace = false;
    }

    public boolean shouldAnalyseWorkspace() {
        return shouldAnalyseWorkspace;
    }

    public void setShouldAnalyseWorkspace(boolean shouldAnalyseWorkspace) {
        this.shouldAnalyseWorkspace = shouldAnalyseWorkspace;
    }

    /**
     * The rectangular bounding box encapsulating the workspace
     *
     * @return the bounding box
     */
    public Box3 getBoundingBox() {
        return boundingBox;
    }

    /**
     * Get a approximation of the workspace volume
     *
     * @return n approximation of the workspace volume
     */
    public double getVolume() {
        return volume;
    }

    /**
     * Get a solid workspace mesh representing this workspace
     *
     * @return a string which really is a .obj 3D model. Can be written to file
     * with the .obj extension and be viewed in any 3D viewer. Requires that
     * MeshLab is added to the PATH
     */
    public String getMesh() {
        if (mesh == null) {
            try {
                mesh = new PointCloudToMeshConverter().getMesh(getBoundaryPointCloud().asPLYFormattedString());
            } catch (IOException ex) {
                Logger.getLogger(Workspace.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return mesh == null ? "Undefined" : mesh;
    }

    /**
     * Computes the workspace using default parameters
     *
     * @return this
     */
    public Workspace compute() {
        LOG.log(Level.INFO, "Computing workspace using default values: c={0}, n={1}", new Object[]{DEFAULT_C, DEFAULT_N});
        return compute(DEFAULT_C, DEFAULT_N);
    }

    /**
     * Calculates the workspace using custom parameters
     *
     * @param c number of random configurations
     * @param n number of voxel along each coordinate axis
     * @return this
     */
    public Workspace compute(int c, int n) {
        TickTock tick = new TickTock();

        this.mesh = null;
        this.volume = 0;
        this.voxels = new Voxel[n - 1][n - 1][n - 1];

        List<DoubleArray> randomJointConfigurations = chain.getRandomJointConfigurations(c);
        List<Vector3d> endEffectorPositions = chain.getEndEffectorPositions(randomJointConfigurations);

        this.boundingBox.computeFromPoints(endEffectorPositions);

        //
        List<ConfigurationCoordinatePair> pairs = new ArrayList<>(c);
        for (int i = 0; i < c; i++) {
            pairs.add(new ConfigurationCoordinatePair(randomJointConfigurations.get(i), endEffectorPositions.get(i)));
        }
        Collections.sort(pairs);

        //
        double[] x = MathUtil.linspace(boundingBox.getMin().getX(), boundingBox.getMax().getX(), n);
        double[] y = MathUtil.linspace(boundingBox.getMin().getY(), boundingBox.getMax().getY(), n);
        double[] z = MathUtil.linspace(boundingBox.getMin().getZ(), boundingBox.getMax().getZ(), n);

        //
        List<List<ConfigurationCoordinatePair>> list = new ArrayList<>(x.length - 1);
        for (int i = 0; i < x.length - 1; i++) {
            List<ConfigurationCoordinatePair> subList = new ArrayList<>();
            list.add(subList);
            for (int j = 0; j < pairs.size(); j++) {
                ConfigurationCoordinatePair v = pairs.get(j);
                if (v.getCoord().getX() <= x[i + 1] && v.getCoord().getX() >= x[i]) {
                    subList.add(v);
                }
            }
        }

        //
        c = 0;
        for (int i = 0; i < n - 1; i++) {
            List<ConfigurationCoordinatePair> list2 = list.get(i);
            for (int j = 0; j < n - 1; j++) {
                for (int k = 0; k < n - 1; k++) {
                    completionService.submit(new Worker1(i, j, k, x, y, z, list2));
                    c++;
                }
            }
        }
        try {
            for (int i = 0; i < c; i++) {
                this.volume += (Double) completionService.take().get();
            }
        } catch (ExecutionException | InterruptedException ex) {
            Logger.getLogger(Workspace.class.getName()).log(Level.SEVERE, null, ex);
        }

        //
        c = 0;
        for (int i = 0; i < n - 1; i++) {
            for (int j = 0; j < n - 1; j++) {
                for (int k = 0; k < n - 1; k++) {
                    completionService.submit(new Worker2(i, j, k, n), null);
                    c++;
                }
            }
        }
        try {
            for (int i = 0; i < c; i++) {
                completionService.take().get();
            }
        } catch (ExecutionException | InterruptedException ex) {
            Logger.getLogger(Workspace.class.getName()).log(Level.SEVERE, null, ex);
        }

        LOG.log(Level.INFO, "Workspace computation took {0}ms. Volume={1}", new Object[]{tick.tock(), this.volume});

        return this;
    }

    public List<Vector3d> getBoundaryNormals() {
        List<Voxel> boundaryVoxels = getBoundaryVoxels();
        List<Vector3d> list = new ArrayList<>();
        boundaryVoxels.stream().forEach((v) -> {
            list.add(v.getNormal());
        });
        return list;
    }

    public List<Vector3d> getBoundaryVertices() {
        List<Voxel> boundaryVoxels = getBoundaryVoxels();
        List<Vector3d> list = new ArrayList<>();
        boundaryVoxels.stream().forEach((v) -> {
            list.add(v.getCenter());
        });
        return list;
    }

    /**
     * Getter for all voxels
     *
     * @return all voxels
     */
    public List<Voxel> getVoxels() {
        List<Voxel> list = new ArrayList<>();
        for (Voxel[][] voxel : voxels) {
            for (Voxel[] voxel1 : voxel) {
                list.addAll(Arrays.asList(voxel1));
            }
        }
        return (list);
    }

    /**
     * Getter for the reachable voxels
     *
     * @return the reachable voxels
     */
    public List<Voxel> getReachableVoxels() {
        List<Voxel> list = new ArrayList<>();
        for (Voxel[][] voxel : voxels) {
            for (Voxel[] voxel1 : voxel) {
                for (Voxel v : voxel1) {
                    if (v.isReachable()) {
                        list.add(v);
                    }
                }
            }
        }
        return (list);
    }

    /**
     * Getter for the inner voxels
     *
     * @return the reachable voxels that are not on the boundary
     */
    public List<Voxel> getInnerVoxels() {
        List<Voxel> list = new ArrayList<>();
        for (Voxel[][] voxel1 : voxels) {
            for (Voxel[] voxel : voxel1) {
                for (Voxel v : voxel) {
                    if (v.isReachable() && !v.isOnBoundary()) {
                        list.add(v);
                    }
                }
            }
        }
        return (list);
    }

    /**
     * Getter for the boundary voxels
     *
     * @return the voxels located at the boundary
     */
    public List<Voxel> getBoundaryVoxels() {
        List<Voxel> list = new ArrayList<>();
        for (Voxel[][] voxel1 : voxels) {
            for (Voxel[] voxel : voxel1) {
                for (Voxel v : voxel) {
                    if (v.isOnBoundary()) {
                        list.add(v);
                    }
                }
            }
        }
        return (list);
    }

    public PointCloud getBoundaryPointCloud() {
        return new PointCloud("BoundaryCloud", getBoundaryVoxels());
    }

    public PointCloud getReachablePointCloud() {
        return new PointCloud("ReacableCloud", getReachableVoxels());
    }

    public PointCloud getGridPointCloud() {
        return new PointCloud("GridCloud", getVoxels());
    }

    public void writePointCloudsToStream(OutputStream os) throws IOException {

        PointCloud boundaryPointCloud = getBoundaryPointCloud();
        PointCloud reachablePointCloud = getReachablePointCloud();
        PointCloud gridPointCloud = getGridPointCloud();

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (ZipOutputStream zos = new ZipOutputStream(baos)) {

            /* File is not on the disk, test.csv indicates
             only the file name to be put into the zip */
            ZipEntry entry1 = new ZipEntry(boundaryPointCloud.getName() + ".ply");

            zos.putNextEntry(entry1);
            zos.write(boundaryPointCloud.asPLYFormattedString().getBytes());
            zos.closeEntry();

            ZipEntry entry2 = new ZipEntry(reachablePointCloud.getName() + ".ply");

            zos.putNextEntry(entry2);
            zos.write(reachablePointCloud.asPLYFormattedString().getBytes());
            zos.closeEntry();

            ZipEntry entry3 = new ZipEntry(gridPointCloud.getName() + ".ply");

            zos.putNextEntry(entry3);
            zos.write(gridPointCloud.asPLYFormattedString().getBytes());
            zos.closeEntry();

        }
        os.write(baos.toByteArray());
        os.flush();
    }

    private class Worker1 implements Callable<Double> {

        private final Color SWLColor = new Color(0, 255, 0);
        private final Color WWLColor = new Color(255, 255, 0);
        private final Color NonSWLAndWWlColor = new Color(255, 0, 0);

        private final double[] WLL = (new double[]{100000, 210000, 50000});
        private final double[] SWL = (new double[]{WLL[0] * 0.85, WLL[1] * 0.85, WLL[2] * 0.85});

        private final int i, j, k;
        private final double[] x, y, z;
        private final List<ConfigurationCoordinatePair> list2;

        public Worker1(int i, int j, int k, double[] x, double[] y, double[] z, List<ConfigurationCoordinatePair> list2) {
            this.i = i;
            this.j = j;
            this.k = k;
            this.x = x;
            this.y = y;
            this.z = z;
            this.list2 = list2;
        }

        @Override
        public Double call() throws Exception {
            Voxel voxel = new Voxel(
                    new Vector3d(x[(i)], y[(j)], z[(k)]),
                    new Vector3d(x[(i + 1)], y[(j + 1)], z[(k + 1)]));
            voxels[i][j][k] = voxel;

            int iswl = 0, iwwl = 0, c = 0;

            for (ConfigurationCoordinatePair pair : list2) {
                Vector3d coord = pair.getCoord();
                DoubleArray conf = pair.getConf();

                if (voxel.containsPoint(coord)) {
                    voxel.setReachable(true);

                    if (!shouldAnalyseWorkspace) {
                        break;
                    } else {
                        c++;
                        double det = chain.getJacobian(conf, false).determinant();
                        if (Math.abs(det) < 0.0001) {
                            voxel.setContainsSingularity(true);
                        }

                        boolean swl = false, wwl = false;
                        DoubleArray tau = chain.getStaticJointTorques(conf, new Vector3d(0, 1000 * -9.81, 0));
                        for (int l = 0; l < tau.size(); l++) {
                            double t = Math.abs(tau.get(l));
                            if (SWL[l] > t) {
                                swl = true;
                                wwl = true;
                            } else if (WLL[l] > t) {
                                swl = false;
                                wwl = true;
                            } else {
                                swl = false;
                                wwl = false;
                                break;
                            }
                        }

                        iswl += swl ? 1 : 0;
                        iwwl += wwl ? 1 : 0;
                    }
                }
            }

            if (iswl > c * 0.75) {
                voxel.setColor(SWLColor);
                voxel.setIsWithinSWL(true);
                voxel.setIsWithinWWL(true);
            } else if (iwwl > c * 0.75) {
                voxel.setColor(WWLColor);
                voxel.setIsWithinSWL(false);
                voxel.setIsWithinWWL(true);
            } else {
                voxel.setColor(NonSWLAndWWlColor);
            }

            return voxel.isReachable() ? (double) voxel.getVolume() : 0;
        }
    }

    private class Worker2 implements Runnable {

        private final int i, j, k, n;

        public Worker2(int i, int j, int k, int n) {
            this.i = i;
            this.j = j;
            this.k = k;
            this.n = n;
        }

        @Override
        public void run() {
            Voxel v = voxels[i][j][k];
            boolean activated = false;
            if (v.isReachable()) {
                Voxel b1, b2;

                if ((i - 1) < 0) {
                    v.setNormal(new Vector3d(-1, 0, 0));
                } else if ((i + 1) == (n - 1)) {
                    v.setNormal(new Vector3d(1, 0, 0));
                }

                if ((j - 1) < 0) {
                    v.setNormal(new Vector3d(0, -1, 0));
                } else if ((j + 1) == (n - 1)) {
                    v.setNormal(new Vector3d(0, 1, 0));
                }

                if ((k - 1) < 0) {
                    v.setNormal(new Vector3d(0, 0, -1));
                } else if ((k + 1) == (n - 1)) {
                    v.setNormal(new Vector3d(0, 0, 1));
                }

                if ((i - 1) >= 0 && (i + 1) < (n - 1)) {
                    b1 = voxels[i - 1][j][k];
                    b2 = voxels[i + 1][j][k];

                    if ((!b1.isReachable()) || (!b2.isReachable())) {
                        activated = true;
                        if (!b1.isReachable()) {
                            v.setNormal(v.getNormal().add(Vector3d.X.multiply(-1)).normalize());
                        } else {
                            v.setNormal(v.getNormal().add(Vector3d.X.multiply(1)).normalize());
                        }
                    }

                } else {
                    activated = true;
                }

                if ((j - 1) >= 0 && (j + 1) < (n - 1)) {
                    b1 = voxels[i][j - 1][k];
                    b2 = voxels[i][j + 1][k];
                    if ((!b1.isReachable()) || (!b2.isReachable())) {
                        activated = true;
                        if (!b1.isReachable()) {
                            v.setNormal(v.getNormal().add(Vector3d.Y.multiply(-1)).normalize());
                        } else {
                            v.setNormal(v.getNormal().add(Vector3d.Y.multiply(1)).normalize());
                        }
                    }

                } else {
                    activated = true;
                }

                if ((k - 1) >= 0 && (k + 1) < (n - 1)) {
                    b1 = voxels[i][j][k - 1];
                    b2 = voxels[i][j][k + 1];
                    if ((!b1.isReachable()) || (!b2.isReachable())) {
                        activated = true;
                        if (!b1.isReachable()) {
                            v.setNormal(v.getNormal().add(Vector3d.Z.multiply(-1)).normalize());
                        } else {
                            v.setNormal(v.getNormal().add(Vector3d.Z.multiply(1)).normalize());
                        }
                    }

                } else {
                    activated = true;
                }

                if (activated) {
                    v.setOnBoundary(true);
                }
            }
        }
    }
}
