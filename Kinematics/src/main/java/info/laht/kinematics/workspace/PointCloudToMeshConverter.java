/*
 * Copyright 2014 Aalesund University College
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */
package info.laht.kinematics.workspace;

import info.laht.utilitypack.utils.FileUtil;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * If MeshLab is installed on the system, this class can be used to convert a
 * point cloud to a solid mesh (.OBJ format)
 *
 * @author Lars Ivar Hatledal
 */
public class PointCloudToMeshConverter {

    private static final Logger LOG = Logger.getLogger(PointCloudToMeshConverter.class.getName());

    private static final int DEFAULT_K = 10;
    private static final int DEFAULT_OCTDEPTH = 9;
    private static final int DEFAULT_SOLVER_DIVIDE = 8;

    /**
     * Builds the mesh using default params
     *
     * @param plyContent the ply file as a string
     * @return the .obj workspace mesh if successfull
     * @throws IOException if an I/O error occurs.
     */
    public String getMesh(String plyContent) throws IOException {
        return getMesh(plyContent, DEFAULT_K, DEFAULT_OCTDEPTH, DEFAULT_SOLVER_DIVIDE);
    }

    /**
     * Builds the mesh using custom params
     *
     * @param plyContent the ply file as a string
     * @param k k setting in the meshlab script
     * @param octDepth octDepth setting in the meshlab script
     * @param solverDivide solverDivide setting in the meshlab script
     * @return the .obj workspace mesh if successfull
     * @throws IOException if an I/O error occurs.
     */
    public String getMesh(String plyContent, int k, int octDepth, int solverDivide) throws IOException {
        Path tmpDir = Files.createTempDirectory("VCPtmpDir");
        Path tmpFile = Files.createTempFile(tmpDir, "cloud", ".ply");
        Path tmpFilter = Files.createTempFile(tmpDir, "PLYmesher", ".mlx");

        LOG.log(Level.FINE, "Created tmpDir {0}", tmpDir);
        LOG.log(Level.FINE, "Created tmpFile {0}", tmpFile);
        LOG.log(Level.FINE, "Created tmpFile {0}", tmpFilter);

        try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(tmpFilter.toFile())))) {
            bw.write(getFilter(k, octDepth, solverDivide));
            bw.flush();
        }

        try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(tmpFile.toFile())))) {
            bw.write(plyContent);
            bw.flush();
        }

        ProcessBuilder pb = new ProcessBuilder(
                "cmd.exe", "/c", "meshlabserver -i "
                + tmpFile.toString() + " -o "
                + tmpDir + File.separator + "mesh.obj -s "
                + tmpFilter.toString() + " -om vc vn");
        pb.redirectErrorStream(true);
        Process p = pb.start();

        StringBuilder sb = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()))) {
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line).append("\n");
            }
        }
        LOG.log(Level.INFO, sb.toString());

        p.destroy();

        String mesh = FileUtil.getTextContent(new File(tmpDir.toString(), "mesh.obj"));

        File objFile = new File(tmpDir + File.separator + "mesh.obj");
        if (Files.deleteIfExists(objFile.toPath())) {
            LOG.log(Level.FINE, "Deleted {0}", objFile.toString());
        }

        if (Files.deleteIfExists(tmpFile)) {
            LOG.log(Level.FINE, "Deleted {0}", tmpFile.toString());
        }

        if (Files.deleteIfExists(tmpFilter)) {
            LOG.log(Level.FINE, "Deleted {0}", tmpFilter.toString());
        }

        if (Files.deleteIfExists(tmpDir)) {
            LOG.log(Level.FINE, "Deleted {0}", tmpDir.toString());
        }

        return mesh;
    }

    private String getFilter(int k, int octDepth, int solverDivide) {

        return "<!DOCTYPE FilterScript>\n"
                + "<FilterScript>\n"
                + " <filter name=\"Smooths normals on a point sets\">\n"
                + "  <Param tooltip=\"The number of neighbors used to smooth normals.\" description=\"Number of neigbors\" type=\"RichInt\" name=\"K\" value=\"" + k + "\"/>\n"
                + "  <Param tooltip=\"If selected, the neighbour normals are waighted according to their distance\" description=\"Weight using neighbour distance\" type=\"RichBool\" name=\"useDist\" value=\"false\"/>\n"
                + " </filter>\n"
                + " <filter name=\"Surface Reconstruction: Poisson\">\n"
                + "  <Param tooltip=\"Set the depth of the Octree used for extracting the final surface. Suggested range 5..10. Higher numbers mean higher precision in the reconstruction but also higher processing times. Be patient.&#xa;\" description=\"Octree Depth\" type=\"RichInt\" name=\"OctDepth\" value=\"" + octDepth + "\"/>\n"
                + "  <Param tooltip=\"This integer argument specifies the depth at which a block Gauss-Seidel solver is used to solve the Laplacian equation.&#xa;Using this parameter helps reduce the memory overhead at the cost of a small increase in reconstruction time. &#xa;In practice, the authors have found that for reconstructions of depth 9 or higher a subdivide depth of 7 or 8 can reduce the memory usage.&#xa;The default value is 8.&#xa;\" description=\"Solver Divide\" type=\"RichInt\" name=\"SolverDivide\" value=\"" + solverDivide + "\"/>\n"
                + "  <Param tooltip=\"This floating point value specifies the minimum number of sample points that should fall within an octree node as the octree&#xa;construction is adapted to sampling density. For noise-free samples, small values in the range [1.0 - 5.0] can be used.&#xa;For more noisy samples, larger values in the range [15.0 - 20.0] may be needed to provide a smoother, noise-reduced, reconstruction.&#xa;The default value is 1.0.\" description=\"Samples per Node\" type=\"RichFloat\" name=\"SamplesPerNode\" value=\"1\"/>\n"
                + "  <Param tooltip=\"This floating point value specifies a correction value for the isosurface threshold that is chosen.&#xa;Values &lt; 1 means internal offsetting, >1 external offsetting.Good values are in the range 0.5 .. 2.&#xa;The default value is 1.0 (no offsetting).\" description=\"Surface offsetting\" type=\"RichFloat\" name=\"Offset\" value=\"1\"/>\n"
                + " </filter>\n"
                + "</FilterScript>";
    }

}
