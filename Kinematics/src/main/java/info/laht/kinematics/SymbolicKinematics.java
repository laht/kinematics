/**
 * Copyright 2014 Aalesund University College
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */
package info.laht.kinematics;

import com.google.gson.Gson;
import info.laht.utilitypack.utils.DoubleArray;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;
import info.laht.kinematics.joints.JointType;
import info.laht.utilitypack.math.Matrix4x4;
import info.laht.utilitypack.math.Vector3d;
import org.ejml.simple.SimpleMatrix;

/**
 * Calculates symbolic kinematic equations by invoking Python
 *
 * @author Lars Ivar Hatledal
 */
public final class SymbolicKinematics {

    private final static Logger LOG = Logger.getLogger(SymbolicKinematics.class.getName());

    private final String json;
    private String fkStr, jStr;
    private String[][] fkMat, jMat;
    private List<JointType> jointTypes;

    public SymbolicKinematics(KinematicChain chain) throws IOException {
        this(chain.getJointTypes(), chain.getRelativePositions());
    }

    public SymbolicKinematics(String[] jointTypes, double[] relativePositions) throws IOException {
        this.json = new ManipulatorJson(jointTypes, relativePositions).toJsonString();
        this.jointTypes = JointType.fromStringArray(jointTypes);
        retrieve();
    }

    public SymbolicKinematics(List<JointType> jointTypes, List<Vector3d> relativePositions) throws IOException {
        this.jointTypes = jointTypes;
        String[] jointTypesStr = JointType.toStringList(jointTypes).toArray(new String[jointTypes.size()]);
        double[] relPositions = new double[relativePositions.size() * 3];
        int i = 0;
        for (Vector3d v : relativePositions) {
            relPositions[i++] = v.getX();
            relPositions[i++] = v.getY();
            relPositions[i++] = v.getZ();
        }

        this.json = new ManipulatorJson(jointTypesStr, relPositions).toJsonString();
        retrieve();
    }

    /**
     * Evaluates the symbolic FK
     *
     * @param arr the joint values to use
     * @return the FK matrix
     */
    public Matrix4x4 evaluateFK(DoubleArray arr) {
        String[] vars = new String[arr.size()];
        for (int i = 0; i < vars.length; i++) {
            vars[i] = "a" + (i + 1);
        }

        SimpleMatrix FK = new SimpleMatrix(4, 4);
        for (int x = 0; x < 4; x++) {
            for (int y = 0; y < 4; y++) {
                String str = fkMat[x][y];
                Expression e = new ExpressionBuilder(str)
                        .variables(vars)
                        .build();
                
                for (int i = 0; i < arr.size(); i++) {
                    JointType jt = jointTypes.get(i);
                    if (JointType.isRevolute(jt)) {
                        e.setVariable(vars[i], Math.toRadians(arr.get(i)));
                    } else if (JointType.isPrismatic(jt)) {
                        e.setVariable(vars[i], (arr.get(i)));
                    } else {
                        throw new IllegalArgumentException();
                    }
                }
                FK.set(x, y, e.evaluate());
            }
        }
        return new Matrix4x4(FK);
    }

    /**
     * Evaluates the symbolic 3xn Jacobian
     *
     * @param arr the joint values to use
     * @return the 3xn (half) jacobian matrix
     */
    public SimpleMatrix evaluateJacobian(DoubleArray arr) {

        String[] vars = new String[arr.size()];
        for (int i = 0; i < vars.length; i++) {
            vars[i] = "a" + (i + 1);
        }

        SimpleMatrix J = new SimpleMatrix(3, arr.size());
        for (int x = 0; x < jMat.length; x++) {
            for (int y = 0; y < jMat[0].length; y++) {
                Expression e = new ExpressionBuilder(jMat[x][y])
                        .variables(vars)
                        .build();
                for (int i = 0; i < arr.size(); i++) {
                    JointType jt = jointTypes.get(i);
                    if (JointType.isRevolute(jt)) {
                        e.setVariable(vars[i], Math.toRadians(arr.get(i)));
                    } else if (JointType.isPrismatic(jt)) {
                        e.setVariable(vars[i], (arr.get(i)));
                    } else {
                        throw new IllegalArgumentException();
                    }
                }
                J.set(x, y, e.evaluate());
            }
        }
        return (J);
    }

    /**
     * The symbolic FK equation
     *
     * @return the symbolic FK equation
     */
    public String getFK() {
        return fkStr;
    }

    /**
     * The symbolic Jacobian equation
     *
     * @return the symbolic Jacobian equation
     */
    public String getJacobian() {
        return jStr;
    }

    private void retrieve() throws IOException {
        Gson gson = new Gson();

        Path tmpDir = Files.createTempDirectory("VCPtmpDir");
        Path tmpFile = Files.createTempFile(tmpDir, "SymKine", ".py");

        LOG.log(Level.FINE, "Created tmpDir {0}", tmpDir);
        LOG.log(Level.FINE, "Created tmpFile {0}", tmpFile);

        try (BufferedWriter bw = new BufferedWriter(new FileWriter(tmpFile.toFile()))) {
            bw.write(getScript());
            bw.flush();
        }

        ProcessBuilder pb = new ProcessBuilder("python", tmpFile.toString());
        pb.redirectErrorStream(true);
        Process p = pb.start();

        String result = "";
        try (BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()))) {
            String str;
            while ((str = br.readLine()) != null) {
                result += str;
            }
        }

        Map msg = gson.fromJson(result, Map.class);

        fkMat = new String[4][4];
        Map fkMap = gson.fromJson((String) msg.get("FK"), Map.class);
        fkStr = (String) fkMap.get("mat");
        String[] fkRows = fkStr.split("\n");
        int i = 0, j = 0;
        for (String str : fkRows) {
            String[] cols = str.split(",");
            for (String str1 : cols) {
                fkMat[i][j] = str1;
                j++;
            }
            j = 0;
            i++;
        }

        jMat = new String[3][3];
        Map jMap = gson.fromJson((String) msg.get("Jacobian"), Map.class);
        jStr = (String) jMap.get("mat");
        String[] rows = jStr.split("\n");
        i = 0;
        j = 0;
        for (String str : rows) {
            String[] cols = str.split(",");
            for (String str1 : cols) {
                jMat[i][j] = str1;
                j++;
            }
            j = 0;
            i++;
        }

        if (Files.deleteIfExists(tmpFile)) {
            LOG.log(Level.FINE, "Deleted {0}", tmpFile.toString());
        }

        if (Files.deleteIfExists(tmpDir)) {
            LOG.log(Level.FINE, "Deleted {0}", tmpDir.toString());
        }
    }

    private String getScript() {
        return "import sys\n"
                + "import json\n"
                + "from sympy import *\n"
                + "\n"
                + "def matToStr(mat):\n"
                + "	s = \"\"\n"
                + "	for i in range(0,mat.rows):\n"
                + "		for j in range(0,mat.cols):\n"
                + "			s+=str(mat[i,j])\n"
                + "			if j != mat.cols-1:\n"
                + "				s+=', '\n"
                + "		s+='\\n'\n"
                + "	return s\n"
                + "	\n"
                + "class KinematicChain:\n"
                + "    \n"
                + "	def __init__(self):\n"
                + "		self.i = 1\n"
                + "		self.variables = []\n"
                + "		self.matrices = []\n"
                + "     \n"
                + "	def add(self, type, relPos):\n"
                + "		m = self.transMatrix(type, relPos);\n"
                + "		self.matrices.append(m)\n"
                + "     \n"
                + "	def fkMat(self):\n"
                + "		fk = eye(4)\n"
                + "		for mat in self.matrices:\n"
                + "			fk = fk * mat\n"
                + "		ans = simplify(fk)\n"
                + "		return ans\n"
                + "     \n"
                + "	def jacobian(self):\n"
                + "		fk = self.fkMat()\n"
                + "		f = Matrix([fk[0,3], fk[1,3], fk[2,3]])\n"
                + "		if (len(self.variables) < 1):\n"
                + "			return eye(4)\n"
                + "		else:\n"
                + "			x = Matrix(self.variables)\n"
                + "			ans = simplify(f.jacobian(x))\n"
                + "			return ans\n"
                + "     \n"
                + "	def transMatrix(self, type, p):\n"
                + "		if (type != \"FIXED\"):\n"
                + "			s1 = \"a\" + str(self.i)\n"
                + "			self.i += 1\n"
                + "			a = symbols(s1)\n"
                + "			self.variables.append(a)\n"
                + "     \n"
                + "		if (type == \"FIXED\"):\n"
                + "			return Matrix([\n"
                + "			[1, 0, 0, p[0]],\n"
                + "			[0, 1, 0, p[1]],\n"
                + "			[0, 0, 1, p[2]],\n"
                + "			[0, 0, 0, 1]])\n"
                + "		elif (type == \"RX\"):\n"
                + "			return Matrix([\n"
                + "			[1, 0, 0, p[0]],\n"
                + "			[0, cos(a), -sin(a), p[1]],\n"
                + "			[0, sin(a), cos(a), p[2]],\n"
                + "			[0, 0, 0, 1]])\n"
                + "		elif (type == \"RY\"):\n"
                + "			return Matrix([\n"
                + "			[cos(a), 0, sin(a), p[0]],\n"
                + "			[0, 1, 0, p[1]],\n"
                + "			[-sin(a), 0, cos(a), p[2]],\n"
                + "			[0, 0, 0, 1]])\n"
                + "		elif (type == \"RZ\"):\n"
                + "			return Matrix([\n"
                + "			[cos(a), -sin(a), 0, p[0]],\n"
                + "			[sin(a), cos(a), 0, p[1]],\n"
                + "			[0, 0, 1, p[2]],\n"
                + "			[0, 0, 0, 1]])\n"
                + "		elif (type == \"PX\"):\n"
                + "			return Matrix([\n"
                + "			[1, 0, 0, p[0] + a],\n"
                + "			[0, 1, 0, p[1]],\n"
                + "			[0, 0, 1, p[2]],\n"
                + "			[0, 0, 0, 1]])\n"
                + "		elif (type == \"PY\"):\n"
                + "			return Matrix([\n"
                + "			[1, 0, 0, p[0]],\n"
                + "			[0, 1, 0, p[1] + a],\n"
                + "			[0, 0, 1, p[2]],\n"
                + "			[0, 0, 0, 1]])\n"
                + "		elif (type == \"PZ\"):\n"
                + "			return Matrix([\n"
                + "			[1, 0, 0, p[0]],\n"
                + "			[0, 1, 0, p[1]],\n"
                + "			[0, 0, 1, p[2] + a],\n"
                + "			[0, 0, 0, 1]])\n"
                + "		else:\n"
                + "			return eye(4)\n"
                + "\n"
                + "if __name__ == '__main__':\n"
                + "	obj = json.loads(" + "\'" + json + "\'" + ")\n"
                + "	\n"
                + "	jointTypes = obj['jointTypes']\n"
                + "	rel = obj['relativePositions']\n"
                + "			\n"
                + "	relativePositions = []\n"
                + "			\n"
                + "	x = 0\n"
                + "	while x < len(rel):\n"
                + "		relativePositions.append([rel[x], rel[x+1], rel[x+2]])\n"
                + "		x += 3\n"
                + "			\n"
                + "	#print message\n"
                + "	FK = KinematicChain()\n"
                + "	for i in range(0, len(jointTypes)):\n"
                + "		FK.add(jointTypes[i], relativePositions[i])\n"
                + "	fk = FK.fkMat()\n"
                + "	j = FK.jacobian()\n"
                + "	print json.dumps({\n"
                + "	'FK':json.dumps({'rows':fk.rows, 'cols':fk.cols, 'mat':matToStr(fk)}),\n"
                + "	'Jacobian':json.dumps({'rows': j.rows, 'cols':j.cols, 'mat':matToStr(j)})})";
    }

    private class ManipulatorJson {

        private final String[] jointTypes;
        private final double[] relativePositions;

        public ManipulatorJson(String[] jointTypes, double[] relativePositions) {
            this.jointTypes = jointTypes;
            this.relativePositions = relativePositions;
        }

        public String toJsonString() {

            return new Gson().toJson(this);
        }
    }
}
