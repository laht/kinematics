/**
 * Copyright 2014 Aalesund University College
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */
package info.laht.kinematics.joints;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * JointType enumerations
 * @author Lars Ivar Hatledal
 */
public enum JointType implements Serializable {

    FIXED {

            },
    RX {

            },
    RY {

            },
    RZ {

            },
    PX {

            },
    PY {

            },
    PZ {

            };

    /**
     * Converts string to corresponding JointType
     * @param str the jointType string. 
     * @return the corresponding JointType
     */
    public static JointType fromString(String str) {
        for (JointType jt : JointType.values()) {
            if (jt.toString().equals(str.toUpperCase())) {
                return jt;
            }
        }
        throw new IllegalArgumentException("String didn't match a enum constant! Was: " + str);
    }

    /**
     * Creates a list of JointTypes from the array of strings
     * @param arr the array of strings
     * @return a list of JointTypes
     */
    public static List<JointType> fromStringArray(String[] arr) {
        List<JointType> list = new ArrayList<>(arr.length);
        for (String str : arr) {
            list.add(fromString(str));
        }
        return list;
    }

    /**
     * Creates a list with the strings representing the enumeration types
     * @param jointTypes the list of jointTypes
     * @return a list of strings
     */
    public static List<String> toStringList(List<JointType> jointTypes) {
        List<String> list = new ArrayList<>(jointTypes.size());
        for (JointType jt : jointTypes) {
            list.add(jt.toString());
        }
        return list;
    }

    /**
     * Is the jointtype actuable
     * @param jt the jointype to check
     * @return true if it is, false otherwise
     */
    public static boolean isActuable(JointType jt) {
        if (jt != null) {
            return (jt != FIXED);
        }
        return false;
    }
    
    /**
     * Is the jointtype revolute
     * @param jt the jointype to check
     * @return true if it is, false otherwise
     */
    public static boolean isRevolute(JointType jt) {
        if (jt != null) {
             return (jt == RX || jt == RY || jt == RZ);
        }
        return false;
    }
    
    /**
     * Is the jointtype prismatic
     * @param jt the jointype to check
     * @return true if it is, false otherwise
     */
    public static boolean isPrismatic(JointType jt) {
        if (jt != null) {
            return (jt == PX || jt == PY || jt == PZ);
        }
        return false;
    }
    
    /**
     * Is the jointtype fixed
     * @param jt the jointype to check
     * @return true if it is, false otherwise
     */
    public static boolean isFixed (JointType jt) {
        if (jt != null) {
            return (jt == FIXED);
        }
        return false;
    }

}
