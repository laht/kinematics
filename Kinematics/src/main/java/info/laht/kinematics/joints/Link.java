/*
 * Copyright 2015 Lars Ivar Hatledal.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package info.laht.kinematics.joints;

import info.laht.utilitypack.math.DualQuaternion;
import info.laht.utilitypack.math.TransformData;
import info.laht.utilitypack.math.Vector3d;
import java.io.Serializable;

/**
 *
 * @author Lars Ivar Hatledal
 */
public class Link implements Serializable, Transform {

    private double mass;
    private Vector3d length;

    public Link(Vector3d axis, double len, double mass) {
        this(axis.multiply(len), mass);
    }

    public Link(Vector3d length, double mass) {
        this.mass = mass;
        this.length = length;
    }

    /**
     * The length to the center of the link
     *
     * @return length to the center of the link
     */
    public Vector3d getCenter() {
        return length.divide(2);
    }

    /**
     * The weight of the link
     *
     * @return the weight
     */
    public double getMass() {
        return mass;
    }

    /**
     * Set the weight of the link
     *
     * @param mass the new weight
     */
    public void setMass(double mass) {
        this.mass = mass;
    }

    /**
     * The length of the link
     *
     * @return the link length
     */
    public Vector3d getLength() {
        return length;
    }

    /**
     * Set the length of the link
     *
     * @param length the new length of the link
     */
    public void setLength(Vector3d length) {
        this.length = length;
    }

    @Override
    public TransformData getTransformation(Double value) {
        if (value != null) {
            throw new IllegalArgumentException("Excpected a null value for links");
        }
        return DualQuaternion.makeTranslationQuaternion(length);
    }

    @Override
    public String toString() {
        return "Link{" + "mass=" + mass + ", length=" + length + '}';
    }
}
