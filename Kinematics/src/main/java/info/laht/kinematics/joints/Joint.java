/*
 * Copyright 2014 Aalesund University College
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package info.laht.kinematics.joints;

import info.laht.utilitypack.math.DualQuaternion;
import info.laht.utilitypack.math.Matrix4x4;
import info.laht.utilitypack.math.TransformData;
import info.laht.utilitypack.math.Vector3d;
import info.laht.utilitypack.math.Limit;
import java.io.Serializable;

/**
 * Joint class
 *
 * @author Lars Ivar Hatledal
 */
public class Joint implements Serializable, Transform{

    private Limit limit;
    private JointType jointType;
    private Vector3d relativePosition;
    private Vector3d axis;

    public Joint(JointType jt, Vector3d relPos) {
        this(jt, relPos, null);
    }

    public Joint(JointType jt, Vector3d relPos, Limit lim) {
        if (jt == null) {
            throw new IllegalArgumentException("jointType cannot be null!");
        }
        if (relPos == null) {
            throw new IllegalArgumentException("relativePosition cannot be null!");
        }
        if (lim == null && jt != JointType.FIXED) {
            throw new IllegalArgumentException("limit cannot be null when joint is actuable!");
        }
        this.jointType = jt;
        this.relativePosition = relPos;
        this.limit = lim;

        if (jt == JointType.PX || jt == JointType.RX) {
            this.axis = Vector3d.X;
        } else if (jt == JointType.PY || jt == JointType.RY) {
            this.axis = Vector3d.Y;
        } else if (jt == JointType.PZ || jt == JointType.RZ) {
            this.axis = Vector3d.Z;
        } else {
            this.axis = null;
        }

    }

    /**
     * This joint's type
     *
     * @return the jointType
     */
    public JointType getJointType() {
        return jointType;
    }

    /**
     * Sets the jointType
     *
     * @param jointType the jointype
     */
    public void setJointType(JointType jointType) {
        this.jointType = jointType;
    }

    /**
     * This joint's relative position to the previous one
     *
     * @return the relative position of this joint to the previous joint
     */
    public Vector3d getRelativePosition() {
        return relativePosition;
    }

    /**
     * Set this joint's realtive position to the previous one
     *
     * @param relativePosition the relative position of this joint to the
     * previous joint
     */
    public void setRelativePosition(Vector3d relativePosition) {
        this.relativePosition = relativePosition;
    }

    /**
     * The joint limit
     *
     * @return the joint limit (lower and upper bound)
     */
    public Limit getLimit() {
        return limit;
    }

    /**
     * Set the limit of the joint
     *
     * @param limit the new limit
     */
    public void setLimit(Limit limit) {
        if (isFixed()) {
            throw new IllegalArgumentException("Fixed type has no limits");
        }
        this.limit = limit;
    }

    /**
     * Get axis of rotation
     *
     * @return the axis of rotation or null if JointType is FIXED
     */
    public Vector3d getAxis() {
        return axis;
    }

    /**
     * Get this joint's transformation matrix
     *
     * @param value the joint value
     * @return the transformation matrix
     */
    public Matrix4x4 getTransformationMatrix(Double value) {
        
        switch (getJointType()) {
            case FIXED:
                return Matrix4x4.makeTranslationMatrix(getRelativePosition());
            case PX:
                return Matrix4x4.makeTranslationMatrix(Vector3d.X.multiply(value).add(relativePosition));
            case PY:
                return Matrix4x4.makeTranslationMatrix(Vector3d.Y.multiply(value).add(relativePosition));
            case PZ:
                return Matrix4x4.makeTranslationMatrix(Vector3d.Z.multiply(value).add(relativePosition));
            case RX:
                return Matrix4x4.makeTransformationMatrix(Vector3d.X, getRelativePosition(), value);
            case RY:
                return Matrix4x4.makeTransformationMatrix(Vector3d.Y, getRelativePosition(), value);
            case RZ:
                return Matrix4x4.makeTransformationMatrix(Vector3d.Z, getRelativePosition(), value);
            default:
                throw new IllegalArgumentException("Unexpected jointType! Was " + getJointType());
        }
    }

    public DualQuaternion getTransformationDualQuaternion(Double value) {
        switch (getJointType()) {
            case FIXED:
                return DualQuaternion.makeTranslationQuaternion(getRelativePosition());
            case PX:
                return DualQuaternion.makeTranslationQuaternion(Vector3d.X.multiply(value).add(relativePosition));
            case PY:
                return DualQuaternion.makeTranslationQuaternion(Vector3d.Y.multiply(value).add(relativePosition));
            case PZ:
                return DualQuaternion.makeTranslationQuaternion(Vector3d.Z.multiply(value).add(relativePosition));
            case RX:
                return DualQuaternion.makeTransformation(Vector3d.X, getRelativePosition(), value);
            case RY:
                return DualQuaternion.makeTransformation(Vector3d.Y, getRelativePosition(), value);
            case RZ:
                return DualQuaternion.makeTransformation(Vector3d.Z, getRelativePosition(), value);
            default:
                throw new IllegalArgumentException("Unexpected jointType! Was " + getJointType());
        }
    }

    @Override
    public TransformData getTransformation(Double value) {
        return getTransformationDualQuaternion(value);
    }

    /**
     * The unit vector related to the joint's axis
     *
     * @return the unit vector describing the axis
     */
    public Vector3d getUnitVector() {
        switch (jointType) {
            case FIXED:
                throw new IllegalArgumentException("Fixes type has no unit vector!");
            case PX:
                return Vector3d.X;
            case PY:
                return Vector3d.Y;
            case PZ:
                return Vector3d.Z;
            case RX:
                return Vector3d.X;
            case RY:
                return Vector3d.Y;
            case RZ:
                return Vector3d.Z;
            default:
                throw new IllegalArgumentException("Unexpected jointType! Was " + jointType);
        }
    }

    /**
     * Is the joint prismatic?
     *
     * @return true if it is, false otherwise
     */
    public boolean isPrismatic() {
        return jointType == JointType.PX || jointType == JointType.PY || jointType == JointType.PZ;
    }

    /**
     * Is the joint revolute?
     *
     * @return true if it is, false otherwise
     */
    public boolean isRevolute() {
        return jointType == JointType.RX || jointType == JointType.RY || jointType == JointType.RZ;
    }

    /**
     * Is the joint actuable?
     *
     * @return true if it is, false otherwise
     */
    public boolean isActuable() {
        return isPrismatic() || isRevolute();
    }

    /**
     * Is the joint fixed?
     *
     * @return true if it is, false otherwise
     */
    public boolean isFixed() {
        return jointType == JointType.FIXED;
    }
}
