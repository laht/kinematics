/*
 * Copyright 2014 Aalesund University College
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package info.laht.kinematics.joints;

import info.laht.utilitypack.math.Vector3d;
import info.laht.utilitypack.scenegraph.Node;
import info.laht.utilitypack.math.Limit;

/**
 * Joint backed by a scenegraph node
 * @author Lars Ivar Hatledal
 */
public class JointNode extends Node {

    private final Joint joint;

    public JointNode(int i, Joint joint) {
        this(i, joint.getJointType(), joint.getRelativePosition(), joint.getLimit());
    }

    public JointNode(int i, JointType jt, Vector3d relPos) {
        this(i, jt, relPos, null);
    }

    public JointNode(int i, JointType jt, Vector3d relPos, Limit lim) {
        super("joint " + i);
        this.joint = new Joint(jt, relPos, lim);
        super.setLocalTranslation(relPos);
    }

    /**
     * Get the joint value. Throw an exception if invoked on a fixed joint
     * @return the joint value in degrees for revolute joints. Metric for prismatic joints
     */
    public double getValue() {
        JointType jointType = joint.getJointType();
        if (jointType == JointType.FIXED) {
            throw new IllegalStateException("Fixed type has no value!");
        }
        
        switch (jointType) {
            case PX:
                return getLocalTransformation().getTranslation().getX();
            case PY:
                return getLocalTransformation().getTranslation().getY();
            case PZ:
                return getLocalTransformation().getTranslation().getZ();
            case RX:
                return Math.toDegrees(getLocalTransformation().getEulerAngles().getX());
            case RY:
                return Math.toDegrees(getLocalTransformation().getEulerAngles().getY());
            case RZ:
                return Math.toDegrees(getLocalTransformation().getEulerAngles().getZ());
            default:
                throw new IllegalArgumentException("Unexpected jointType! Was: " + jointType);
        }
    }

    /**
     * Sets the joint value. Will throw an exception if called on a fixed joint
     * @param value the metric value for prismatic joints, radians for revolute ones.
     */
    public void setValue(double value) {
        JointType jointType = joint.getJointType();
        Vector3d relPos = joint.getRelativePosition();
        if (jointType == JointType.FIXED) {
            throw new IllegalArgumentException("Cannot set the value of a fixed joint");
        }
        double val = joint.getLimit().clamp(value);
        switch (jointType) {
            case PX:
                setLocalTranslation(new Vector3d(relPos.getX() + val, relPos.getY(), relPos.getZ()));
                break;
            case PY:
                setLocalTranslation(new Vector3d(relPos.getX(), relPos.getY() + val, relPos.getZ()));
                break;
            case PZ:
                setLocalTranslation(new Vector3d(relPos.getX(), relPos.getY(), relPos.getZ() + val));
                break;
            case RX:
                setLocalRotation(Vector3d.X, val);
                break;
            case RY:
                setLocalRotation(Vector3d.Y, val);
                break;
            case RZ:
                setLocalRotation(Vector3d.Z, val);
                break;
            default:
                throw new IllegalArgumentException("Unexpected jointType! Was: " + jointType);
        }
    }
}
