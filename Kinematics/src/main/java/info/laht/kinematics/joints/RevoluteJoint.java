/*
 * Copyright 2015 laht.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package info.laht.kinematics.joints;

import info.laht.utilitypack.math.DualQuaternion;
import info.laht.utilitypack.math.TransformData;
import info.laht.utilitypack.math.Vector3d;
import info.laht.utilitypack.math.Limit;

/**
 *
 * @author laht
 */
public class RevoluteJoint extends AbstractJoint {

    public RevoluteJoint(Vector3d axis, Limit limit) {
        super(axis, limit);
    }

    @Override
    public TransformData getTransformation(Double value) {

        if (value == null) {
            throw new IllegalArgumentException("value cannot be null!");
        }

        Vector3d axis = getAxis();

        if (axis == Vector3d.X) {
            return DualQuaternion.makeRotation(Vector3d.X, value);
        } else if (axis == Vector3d.Y) {
            return DualQuaternion.makeRotation(Vector3d.Y, value);
        } else if (axis == Vector3d.Z) {
            return DualQuaternion.makeRotation(Vector3d.Z, value);
        } else {
            throw new IllegalStateException("Unexpected axis! Was: " + axis);
        }

    }

}
