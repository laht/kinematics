/*
 * Copyright 2015 laht.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package info.laht.kinematics.joints;

import info.laht.utilitypack.math.Vector3d;
import info.laht.utilitypack.math.Limit;

/**
 *
 * @author laht
 */
public abstract class AbstractJoint implements Transform {

    private final Vector3d axis;
    private Limit limit;
    private Link prevLink, nextLink;

    public AbstractJoint(Vector3d axis, Limit limit) {
        this.axis = axis;
        this.limit = limit;
    }

    public Vector3d getAxis() {
        return axis;
    }

    public Limit getLimit() {
        return limit;
    }

    public void setLimit(Limit limit) {
        this.limit = limit;
    }

}
