/*
 * Copyright 2014 Aalesund University College
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package info.laht;

import info.laht.utilitypack.math.Limit;
import java.io.IOException;
import info.laht.kinematics.joints.JointType;
import info.laht.kinematics.KinematicChain;
import info.laht.utilitypack.math.Vector3d;
import info.laht.utilitypack.utils.FileUtil;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Lars Ivar Hatledal
 */
public class Main {
    private static final Logger LOG = Logger.getLogger(Main.class.getName());
    
    private static int[] c = new int[]{50000, 150000, 300000, 500000};//, 1000000, 1800000};
    private static int[] n = new int[]{15, 25, 35, 45};//, 55, 65};

    public static void main(String[] args) throws IOException {
//        KinematicChain chain = new KinematicChain("Crane3R");
//        chain.addJoint(JointType.RY, Vector3d.ZEROS, new Limit(-180, 180));
//        chain.addJoint(JointType.RX, Vector3d.Y.multiply(2.61), new Limit(-92, 0));
//        chain.addJoint(JointType.RX, Vector3d.Z.multiply(7.01), new Limit(45, 150));
//        chain.addFixedJoint(Vector3d.Z.multiply(3.61));
        
        
         KinematicChain chain = new KinematicChain("Crane3R");
        chain.addJoint(JointType.RY, Vector3d.ZEROS, new Limit(-180, 180));
        chain.addJoint(JointType.RX, Vector3d.Y.multiply(2.2), new Limit(-60, 0));
        chain.addJoint(JointType.RX, Vector3d.Z.multiply(7.0), new Limit(53, 150));
        chain.addFixedJoint(Vector3d.Z.multiply(5));
        
        chain.setShouldAnalyseWorkspace(true);
        for (int i = 0; i < c.length; i++) {
            LOG.log(Level.INFO, "c={0}, n={1}", new Object[]{c[i], n[i]});
            chain.computeWorkspace(c[i], n[i]);
            
            System.out.println("");
            
           // if (i == c.length-1) {
                FileUtil.writeToDisk(chain.getWorkspace().getBoundaryPointCloud().asPLYFormattedString(), new File("BoundaryCloud" + i + ".ply"));
                FileUtil.writeToDisk(chain.getWorkspace().getReachablePointCloud().asPLYFormattedString(), new File("ReachableCloud" + i + ".ply"));
                FileUtil.writeToDisk(chain.getWorkspace().getGridPointCloud().asPLYFormattedString(), new File("GridCloud" + i + ".ply"));
           // }
            
        }
        
        
        //chain.computeWorkspace(300000, 35);
        
//        DoubleArray vals = new DoubleArray(new double[]{-90, -50, 100});
//        
//        KinematicNodeChain nodeChain = new KinematicNodeChain(chain);
//        
//        
//        
//        
//        nodeChain.setJointValues(vals);
//        System.out.println(chain.getTransformationQuaternions(null, new DoubleArray(3)));
//        nodeChain.setJointValues(new DoubleArray(3));
//        System.out.println(nodeChain.getTransformations());

       
//      
//        System.out.println(nodeChain.getJointValues());
////
//        for (DualQuaternion q : nodeChain.getTransformations()) {
//            System.out.println(q.getTranslation());
//        }
        
//        for (DualQuaternion q : nodeChain.getTransformationQuaternions(null, vals)) {
//            System.out.println(q.getTranslation());
//        }
//        
//        for (Matrix4x4 m : nodeChain.getTransformationMatrices(null, vals)) {
//            System.out.println(m);
//        }
////        
//        System.out.println(nodeChain.getEndEffectorState().getMatrixRepresentation());
//        System.out.println(chain.getEndEffectorState(null, vals).getMatrixRepresentation());
//        System.out.println(chain.getStates(null, vals));
        
//        System.out.println(chain.evaluateFK(vals));
//        
//        System.out.println(chain.getJacobian(vals, false));
//        System.out.println(chain.evaluateJacobian(vals));
//        
//        
//        System.out.println(chain.getSymbolicFK());
//        System.out.println(chain.getSymbolicJacobian());
//        
//        Vector3d target = chain.denormalize(new Vector3d(0.5,0.5,0.8));
//        System.out.println(target);
//        System.out.println(chain.getTransformationMatrix(null, chain.solveIK(null, target, 0.01, 20)).getTranslation());

       
//        
//        FileUtil.writeToDisk(workspace.getMesh(), new File("mesh.obj"));
//        FileUtil.writeToDisk(workspace.getPointCloudString(true), new File("boundary.ply"));
//        FileUtil.writeToDisk(workspace.getPointCloudString(false), new File("reachable.ply"));
        
        LOG.log(Level.INFO, "Done");
        
        System.exit(0);
    }
}
